#!/usr/bin/env bash

. lib/bash-ui

# --- Funções dos testes ------------------------------------------------------

janela_principal() {
    tput civis
    cls
    draw_window "$@" $COMPACTO
    [[ $COMPACTO ]] && set_home || set_line 1
    text_left 'BASH_UI'
    text_right 'DEBXP/CURSOS'
    tput cnorm
}

janela_menu1() {
    tput civis
    cls
    janela_principal 'ESTE É UM DIÁLOGO DE MENU'
    tput cnorm
    dialog_menu "$1" $COMPACTO
    tput civis
    janela_principal 'RESPOSTA'
    tput cnorm
    dialog_msg "Você digitou $REPLY..." $COMPACTO
}

janela_msg1() {
    tput civis
    cls
    janela_principal 'ESTE É UM DIÁLOGO DE MENSAGENS'
    tput cnorm
    dialog_msg "$1" $COMPACTO
}

janela_yn() {
    tput civis
    cls
    janela_principal 'ESTE É UM DIÁLOGO SIM/NÃO'
    tput cnorm
    dialog_yn "$1" $COMPACTO
}

# --- Mensagens e menus -------------------------------------------------------

msg="Olá mundo!

Esta é uma mensagem criada para demonstrar
as possibilidades desta caixa de mensagens!

Não é legal? ;-)"


menu="\
1: Opção 1
2: Opção 2
3: Opção 3
4: Opção 4

Q: Sair"

# --- Rotina principal --------------------------------------------------------

# COMPACTO='compact'

janela_principal 'TESTE DE COMPONENTES'

show_command ' Tecle algo para continuar... ' $COMPACTO

janela_menu1 "$menu"

janela_msg1 "$msg"

janela_yn "Podemos continuar com o procedimento?"

janela_principal 'TESTE DE COMPONENTES'

show_command ' Tecle algo para sair... ' $COMPACTO

cls
